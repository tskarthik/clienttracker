# name installer
Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "SimpleTricksDeploy.exe"
InstallDir "C:\SimpleTricks"
ShowInstDetails show

Section -SETTINGS
  SetOutPath "$INSTDIR"
  SetOverwrite ifnewer
;  Var /GLOBAL switch_overwrite
;  StrCpy $switch_overwrite 1
  
SectionEnd

section "install"
    writeUninstaller "$INSTDIR\uninstall.exe"
sectionEnd

Section "Python" SEC02
  File "prerequisite\python-2.7.2.msi"
  File "prerequisite\Setup-Subversion-1.7.3.msi"
  ExecWait 'msiexec /i "$INSTDIR\python-2.7.2.msi" /passive ALLUSERS=1'
  IfErrors error_installing_python
  ExecWait 'msiexec /i "$INSTDIR\Setup-Subversion-1.7.3.msi" /passive ALLUSERS=1'
  IfErrors error_installing_python
  goto done_python
  
  error_installing_python:
    Abort
  done_python:
    ReadEnvStr $R0 "PATH"
    StrCpy $R0 "$R0;C:\Python27\;C:\Python27\Scripts\;C:\Python27\Lib\site-packages\django\bin;"
    SetEnv::SetEnvVar "PATH" $R0
    #messageBox MB_OK "python is done"
;    Delete $INSTDIR\python-2.7.2.msi
SectionEnd

Section "python_setup_tools" SEC03
  File "prerequisite\setuptools-0.6c11.win32-py2.7.exe"
  ExecWait '"$INSTDIR\setuptools-0.6c11.win32-py2.7.exe" /SP- /SILENT'
  IfErrors error_installing_python_setup_tools
  goto done_python_setup_tools
  
  error_installing_python_setup_tools:
    messageBox MB_OK "Problem in setup"
    Abort
  done_python_setup_tools:
    #messageBox MB_OK "python setup tools is done"
;    Delete $INSTDIR\python-2.7.2.msi
SectionEnd

Section "easy_install_django" SEC04
  ExecWait '"C:\Python27\Scripts\easy_install" django'
  IfErrors error_django
  goto done_django
  
  error_django:
    messageBox MB_OK "Problem in django setup"
    Abort
  done_django:
    #messageBox MB_OK "django setup is done"
    ExecWait '"C:\Python27\Scripts\easy_install" PIL'
    IfErrors error_pil
    goto done_pil
  error_pil:
    messageBox MB_OK "Problem in PIL setup"
    Abort
  done_pil:
    #messageBox MB_OK "pil setup is done"
    ExecWait '"C:\Python27\Scripts\easy_install" django_uni_form'
    IfErrors error_uniform
    goto done_uniform
  error_uniform:
    messageBox MB_OK "Problem in uniform setup"
    Abort
  done_uniform:
    #messageBox MB_OK "uniform setup is done"
    ExecWait '"C:\Python27\Scripts\easy_install" south'
    IfErrors error_south
    goto done_south
  error_south:
    messageBox MB_OK "Problem in south setup"
    Abort
  done_south:
    ExecWait '"C:\Python27\Scripts\easy_install" pip'
    ExecWait '"C:\Python27\Scripts\pip" install svn+http://dojango-datable.googlecode.com/svn/trunk/'
    IfErrors error_dojango-datable
    goto done_dojango-datable
  error_dojango-datable:
    messageBox MB_OK "Problem in dojango-datable"
    Abort
  done_dojango-datable:
    #messageBox MB_OK "dojango-datable is done"

SectionEnd


!define FileCopy `!insertmacro FileCopy`
!macro FileCopy FilePath TargetDir
  CreateDirectory `${TargetDir}`
  CopyFiles `${FilePath}` `${TargetDir}`
!macroend

Function openLinkNewWindow
  Push $3
  Exch
  Push $2
  Exch
  Push $1
  Exch
  Push $0
  Exch
 
  ReadRegStr $0 HKCR "http\shell\open\command" ""
# Get browser path
    DetailPrint $0
  StrCpy $2 '"'
  StrCpy $1 $0 1
  StrCmp $1 $2 +2 # if path is not enclosed in " look for space as final char
    StrCpy $2 ' '
  StrCpy $3 1
  loop:
    StrCpy $1 $0 1 $3
    DetailPrint $1
    StrCmp $1 $2 found
    StrCmp $1 "" found
    IntOp $3 $3 + 1
    Goto loop
 
  found:
    StrCpy $1 $0 $3
    StrCmp $2 " " +2
      StrCpy $1 '$1"'
 
  Pop $0
  Exec '$1 $0'
  Pop $0
  Pop $1
  Pop $2
  Pop $3
FunctionEnd
 
!macro _OpenURL URL
Push "${URL}"
Call openLinkNewWindow
!macroend

!include "winver.nsh"
;!include 'FileFunc.nsh'
;!insertmacro Locate
;!include 'MoveFileFolder.nsh'

Section "unveil_application" SEC05
  File "prerequisite\mysite.zip"  
  File "prerequisite\httpd.conf"
  File "prerequisite\httpd.conf_w7"
  File "prerequisite\mod_wsgi.so"
  
  nsUnzip::Extract "$INSTDIR\mysite.zip" /END
 
  IfErrors error_unveil_application
  goto done_unveil_application

  error_unveil_application:
    messageBox MB_OK "Problem in unpack"
    Abort
  done_unveil_application:
    Pop $0
    #MessageBox MB_OK "Simple unpack.$\nResult: $0"
    ${If} ${IsWinXP}
      DetailPrint "Running on Windows XP."
      ReadRegStr $R0 HKLM \
        "SOFTWARE\Microsoft\Windows NT\CurrentVersion" SystemRoot
      StrCmp $R0 "C:\Windows" 0
      #IfErrors incorrect_sys_dir

      ${FileCopy} "$INSTDIR\httpd.conf" "C:\Program Files\Apache Software Foundation\Apache2.2\conf\"    
      ${FileCopy} "$INSTDIR\mod_wsgi.so" "C:\Program Files\Apache Software Foundation\Apache2.2\modules\" 
    ${EndIf}
    ${If} ${IsWin7}
      DetailPrint "Running on Windows 7."
      messageBox MB_OK "Not configured properly for windows 7"
      #!insertmacro MoveFile "$INSTDIR\httpd.conf_w7" "C:\Program Files (x86)\Apache Software Foundation\Apache2.2\conf\httpd.conf"
      ${FileCopy} "$INSTDIR\mod_wsgi.so" "C:\Program Files (x86)\Apache Software Foundation\Apache2.2\modules\" 
    ${EndIf}
    Delete $INSTDIR\mysite.zip
    
;  incorrect_sys_dir:
;    messageBox MB_OK "Not configured properly for windows xp since system directory is not C:"
SectionEnd

!Macro "CreateURL" "URLFile" "URLSite" "URLDesc"
  WriteINIStr "$INSTDIR\${URLFile}.URL" "InternetShortcut" "URL" "${URLSite}"
  SetShellVarContext "all"
  WriteINIStr "$DESKTOP\${URLFile}.URL" "InternetShortcut" "URL" "${URLSite}"
!macroend
 
Section "Apache" SEC06
  File "prerequisite\httpd-2.2.22-win32-x86-no_ssl.msi"
  
  ExecWait 'msiexec /i "$INSTDIR\httpd-2.2.22-win32-x86-no_ssl.msi" /passive ALLUSERS=1 SERVERADMIN=admin@localhost SERVERNAME=localhost SERVERDOMAIN=localhost SERVERPORT=80'
  IfErrors error_installing_apache
  goto done_apache
  
  error_installing_apache:
    Abort
  done_apache:
    #messageBox MB_OK "apache is done"
 ;  Delete $INSTDIR\httpd-2.2.22-win32-x86-no_ssl.msi
  !insertmacro "CreateURL" "Simple Tricks Website" "http://localhost:2090" "Visit SimpleTricks Website"
  !insertmacro "_OpenURL" "http://localhost:2090"
SectionEnd

Function un.RMDirUP
     !define RMDirUP '!insertmacro RMDirUPCall'
 
         !macro RMDirUPCall _PATH
                push '${_PATH}'
                Call un.RMDirUP
         !macroend
 
         ; $0 - current folder
         ClearErrors
 
         Exch $0
         ;DetailPrint "ASDF - $0\.."
     RMDir "$0\.."
 
         IfErrors Skip
         ${RMDirUP} "$0\.."
         Skip:
 
         Pop $0
FunctionEnd

# uninstaller section start
section "uninstall"
    Delete "$INSTDIR\uninstall.exe"
    ExecWait 'msiexec /x "$INSTDIR\httpd-2.2.22-win32-x86-no_ssl.msi" /passive' 
    ExecWait 'msiexec /x "$INSTDIR\python-2.7.2.msi" /passive'
    ExecWait 'msiexec /x "$INSTDIR\Setup-Subversion-1.7.3.msi" /passive'
    Delete $INSTDIR\httpd-2.2.22-win32-x86-no_ssl.msi
    Delete $INSTDIR\python-2.7.2.msi
    Delete $INSTDIR\setuptools-0.6c11.win32-py2.7.exe
    RMDir /r "$INSTDIR"
    Delete "$DESKTOP\Shortcut to SimpleTricks.lnk"
sectionEnd
