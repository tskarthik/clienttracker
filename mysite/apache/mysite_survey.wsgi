import os, sys

#Calculate the path based on the location of the WSGI script.
apache_configuration= os.path.dirname(__file__)
project = os.path.dirname(apache_configuration)
workspace = os.path.dirname(project)
sys.path.append(workspace) 

#Add the path to 3rd party django application and to django itself.
#sys.path.append('C:\\yml\\_myScript_\\dj_things\\web_development\\svn_views\\django_src\\trunk')
sys.path.insert(0, 'C:\\Users\\KARTHIK\\workspace\\repos\\smallshop\\mysite\\dojango\\')
sys.path.insert(0, 'C:\\Users\\KARTHIK\\workspace\\repos\\smallshop\\mysite\\tracker\\')
sys.path.insert(0, 'C:\\Users\\KARTHIK\\workspace\\repos\\smallshop\\mysite\\')
sys.path.insert(0, 'C:\\Users\\KARTHIK\\workspace\\repos\\smallshop\\mysite\\apache')


os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.apache.settings_production'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

print >> sys.stderr, sys.path
