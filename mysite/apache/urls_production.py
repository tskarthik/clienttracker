from django.conf.urls.defaults import patterns, include, url
from mysite.tracker import views
import mysite.settings



# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^dojango/', include('dojango.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', views.home),
                       url(r'^content/', views.content),
                       url(r'^addcustomer/$', views.add_customer),
                       url(r'^search/$', views.search),
                       #support upto 9999999 customers
                       url(r'^search/(\d{1,7})/$',
                               views.edit_customer),
                       url(r'^editcustomer/(\d{1,7})/$',
                               views.edit_customer),
                       url(r'^deletecustomer/(\d{1,7})/$',
                               views.delete_customer),
                       
                       )


if mysite.settings.DEBUG:
    urlpatterns += patterns('django.views.static',
                            (r'^%s(?P<path>.*)$' % (mysite.settings.MEDIA_URL[1:],) , 
                             'serve',{'document_root' : mysite.settings.MEDIA_ROOT, 'show_indexes' : True }),)
    
