from django.contrib import admin
from mysite.tracker.models import Customer, ClassPreference, ContactPreference, Signal, Service

class CustomerAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'mobile1', 'email1', 'class_preference', 'contact_preference')
    search_fields = ('name', 'city')
#    date_hierarchy = 'period_of_signal'
#    ordering = ('-period_of_signal',)
#    raw_id_fields = ('signal',)

admin.site.register(Customer, CustomerAdmin)
admin.site.register(ClassPreference)
admin.site.register(ContactPreference)
admin.site.register(Signal)
admin.site.register(Service)


