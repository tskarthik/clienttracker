from django.db import models

class Signal(models.Model):
    name = models.CharField(max_length=255)
    identifier = models.IntegerField(db_index=True)
    from_period = models.DateField(auto_now_add=True)
    expires_on = models.DateField()

    def __unicode__(self):
        return u"%s %s" % (self.name, self.expires_on)

class ContactPreference(models.Model):
    mode_of_contact = models.CharField(max_length=255)

    def __unicode__(self):
        return self.mode_of_contact

class ClassPreference(models.Model):
    type_pref = models.CharField(max_length=255)

    def __unicode__(self):
        return self.type_pref

class Service(models.Model):
    name = models.CharField(max_length=255)
    identifier = models.IntegerField(db_index=True)
    from_period = models.DateField(auto_now_add=True)
    expires_on = models.DateField()

    def __unicode__(self):
        return u"%s %s" % (self.name, self.expires_on)

# Create your models here.
class Customer(models.Model):
    name = models.CharField(max_length=255)
    address = models.CharField(blank=True, max_length=255)
    city = models.CharField(max_length=255)
    ipaddress = models.IPAddressField(db_index=True)
    mobile1 = models.CharField(max_length=50)
    mobile2 = models.CharField(blank=True, max_length=50)
    landline1 = models.CharField(max_length=50)
    landline2 = models.CharField(blank=True, max_length=50)
    email1 = models.EmailField()
    email2 = models.EmailField(blank=True)    
    signal = models.ManyToManyField(Signal)
    contact_preference = models.ForeignKey(ContactPreference)
    class_preference = models.ForeignKey(ClassPreference)
    service = models.ManyToManyField(Service)
    photo = models.ImageField(upload_to='photos', null=True, blank=True)


