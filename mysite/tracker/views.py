from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from mysite.tracker.models import Customer, Service, Signal, ClassPreference
from mysite.tracker.forms import RegisterForm
from django.core.files.base import ContentFile
from datetime import datetime
from django.utils.translation import ugettext as _
from datable.core.serializers import HrefSerializer
from datable.core.serializers import URLSerializer

from django.db.models import Q

from datable.web.table import Table
from datable.web.storage import Storage

from datable.core.converters import IntegerConverter
from datable.web.widgets import Maximum, Minimum
from datable.web.widgets import Constraints
from datable.web.widgets import Widget
from datable.web.widgets import StringWidget
from datable.web.widgets import BooleanWidget
from datable.web.widgets import DateWidget
from datable.web.widgets import DateTimeWidget

from datable.core.serializers import UnicodeSerializer
from datable.core.serializers import FormatStringSerializer

from datable.core.filters import IntegerFilter

from datable.web.extra.widgets import AutocompleteStringWidget
from datable.web.extra.widgets import DateTimeLessOrEqual
from datable.web.extra.widgets import ForeignKeyComboBox
from datable.web.extra.widgets import DateTimeGreaterOrEqual
from datable.web.extra.widgets import PeriodicRefreshWidget
from datable.core.converters import DojoComboValueConverter
from datable.core.filters import StringFilter

from datable.web.columns import Column
from datable.web.columns import StringColumn
from datable.web.columns import DateColumn
from datable.web.columns import BooleanColumn
from datable.web.columns import DateTimeColumn
from datable.core.serializers import ForeignKeySerializer
from datable.core.serializers import StringSerializer
from datable.core.serializers import DateSerializer

def home(request):
    return render_to_response('tracker/index.html')

def content(request):
    customers_count = Customer.objects.all().count()
    customers_recently_added = Customer.objects.all().order_by('-id')[:3]
    return render_to_response('tracker/content.html',
                              {'customers_count':customers_count,
                               'customers_recently_added':customers_recently_added})

def add_customer(request):
    form = RegisterForm(request.POST or None)
    if form.is_valid():
        if 'photo' in request.FILES.keys():
            file_content = ContentFile(request.FILES['photo'].read())
        
        cmodel = form.save()
        if 'photo' in request.FILES.keys():
            cmodel.photo.save(request.FILES['photo'].name, file_content)
        cmodel.save()        
        return redirect(content)
    
    return render_to_response('tracker/add_customer.html',
                              {'contact_form':form},
                               context_instance=RequestContext(request))


class ServiceSerializer(UnicodeSerializer):
    def serialize(self, model, output_format=None):
        return ", ".join([
            unicode(service)
            for service in model.service.all()
            ])

class SignalSerializer(UnicodeSerializer):
    def serialize(self, model, output_format=None):
        return ", ".join([
            unicode(signal)
            for signal in model.signal.all()
            ])


def search(request):
        first_table = Table(
        name='first_table',

        storage=Storage(
            querySet=Customer.objects.all(),
            columns=[
                StringColumn('id', width=20),
                StringColumn('name', width=100),
                StringColumn('city', width=70),
                StringColumn('contact_preference', width=70),
                StringColumn('ipaddress', width=100),
                Column(
                    'service',
                    width=250,
                    sortable=False,
                    serializer=ServiceSerializer()
                ),                     
                Column(
                    'signal',
                    width=250,
                    sortable=False,
                    serializer=SignalSerializer()
                ),
                StringColumn('mobile1', width=70),
                StringColumn('mobile2', width=70),
                StringColumn('landline1', width=70),
                StringColumn('landline2', width=70),
                StringColumn('email1', width=120),
                StringColumn('email2', width=120),
                #DateColumn('published_on', width=100),
                # DateTimeColumn('created_on', width=150),
                # BooleanColumn('approved', width=30),
                ],
            widgets=[
                StringWidget('name', placeholder=_("Name")),
                StringWidget('city', placeholder=_("City")),
                StringWidget('ipaddress', placeholder=_("ipaddress")),
                StringWidget('email1', placeholder=_("Email1")),
                StringWidget('email2', placeholder=_("Email2")),
                StringWidget('mobile1', placeholder=_("Mobile1")),
                StringWidget('mobile2', placeholder=_("Mobile2")),
                StringWidget('landline1', placeholder=_("landline1")),
                StringWidget('landline2', placeholder=_("landline2")),
#                StringWidget('signal__name', placeholder=_("Signal name")),
#                StringWidget('signal__id', placeholder=_("Signal id")),
                ForeignKeyComboBox(
                                   'service', 
                                   otherSet=Service.objects.all(), 
                                   otherField='name_service expires_on_service', 
                                   otherFormat='%(name)s %(expires_on)s',
                                   placeholder=_("service")),
                ForeignKeyComboBox(
                                   'signal', 
                                   otherSet=Signal.objects.all(), 
                                   otherField='name_signal expires_on_signal', 
                                   otherFormat='%(name)s %(expires_on)s',
                                   placeholder=_("signal")),

                #DateWidget('service__expires_on', placeholder=_('Service expires on')),

                     
                     ]),
                            
            filename=_("My important export data %Y-%m-%d")
            )
        if first_table.willHandle(request):
            return first_table.handleRequest(request)

        return render_to_response(
                                  "tracker/search.html", {'first_table': first_table})

    # context = {}
    # return render_to_response('tracker/search.html',
    #                           context)

def edit_customer(request, customer_id):
    cust = get_object_or_404(Customer, pk=customer_id)
    image_url = "/photo/" + str(cust.photo)
    form = RegisterForm(request.POST or None, instance=cust)
    
    if form.is_valid():
        if 'photo' in request.FILES.keys():
            file_content = ContentFile(request.FILES['photo'].read())

        cmodel = form.save()
        
        if 'photo' in request.FILES.keys():
            cmodel.photo.save(request.FILES['photo'].name, file_content)
        #this is where you might choose to do stuff.
        cmodel.save()
        return redirect(search)

    return render_to_response('tracker/edit_customer.html',
                              {'contact_form': form,
                               'customer_id': customer_id,
                               'image_url': image_url},
                              context_instance=RequestContext(request))
    
def delete_customer(request, customer_id):
    cust = get_object_or_404(Customer, pk=customer_id)
    cust.delete()
    return redirect(search)
    
